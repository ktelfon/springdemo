package exmaple.places;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import exmaple.robot.Robot;

@Component
public class Street {

    @Autowired
    private Robot robot;

    public void show(){
        robot.greet();
    }
}
