package exmaple.robot;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:application.properties")
public class Lazor implements Weapon{

    @Value("${lazor.color}")
    private String color;

    public Lazor() {
        this.color = color;
    }

    public String getColor() {
        return color;
    }
}
