package exmaple.robot;

import org.springframework.stereotype.Component;

@Component(value="knife")
public class Knife implements Weapon {
    @Override
    public String getColor() {
        return "chrome";
    }
}
