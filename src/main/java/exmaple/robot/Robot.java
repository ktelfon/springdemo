package exmaple.robot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Robot {
    private String name;
    private String weaponType;
    @Autowired
    @Qualifier("knife")
    private Weapon weapon;

    public void greet() {
        System.out.println("Hello my name is " + name);
        if (weapon != null)
            System.out.println("I have a lazor it's color" + weapon.getColor());
    }

    public void setName(String name) {
        this.name = name;
    }
}
