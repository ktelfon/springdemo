import configuration.AppConf;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import exmaple.places.Home;
import exmaple.places.Street;
import exmaple.robot.Robot;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext();
        context.register(AppConf.class);
        context.refresh();

        Robot r = context.getBean(Robot.class);
        r.greet();
        Home home = context.getBean(Home.class);
        home.show();
        Street street = context.getBean(Street.class);
        street.show();
    }
}
